#!/usr/bin/env python3

import os
import pika
from datetime import datetime


rabbit_user = os.environ.get('RABBITMQ_USER', 'admin')
rabbit_pass = os.environ.get('RABBITMQ_PASS', 'admin')
rabbit_addr = os.environ.get('RABBITMQ_ADDR')
rabbit_port = int(os.environ.get('RABBITMQ_PORT', '5672'))
credentials = pika.PlainCredentials(rabbit_user, rabbit_pass)
connection = pika.BlockingConnection(pika.ConnectionParameters(rabbit_addr, rabbit_port, '/', credentials))

channel = connection.channel()

channel.queue_declare(queue='response')

start_time = datetime.now()

def callback(ch, method, properties, body):
    if body:
        print(body)
        print('time: %s' % (datetime.now() - start_time))
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume('response', callback)


channel.start_consuming()
