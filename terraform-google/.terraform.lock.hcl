# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version = "3.66.1"
  hashes = [
    "h1:oyVU+NXuJovmCHpyj9vvuUnDxgGFNN+HyCgbIBoEcug=",
    "zh:18c6e686439f2b8ab054cc22ddd42737f0e9cc193798e7bdbf230ce8a64c0e57",
    "zh:400fad5207b606972b91b0f6cb599b79e2f473435760236780001eb11f4f88a5",
    "zh:46e4fb036ab56067ca4c121a89bde23ddd4c50e3e696d2d826efa8583d7c8845",
    "zh:48438bf8be72adec0bc3ee23eca05db2ab772f4a7e31d22cccf217e0ddb7b316",
    "zh:4b2d88ff13624b7a9e5bb1b84e290cb6cc64090e0d29e54b536a0d931ebdaa56",
    "zh:5485b05bc3d03319f4d11fa53821f446ba3d26e500e616fc4bd4248de41cb637",
    "zh:a45ef1cc10a7eac6cffe8f7c84e07f05e2db6c56645141a58c8ebb3cbe25a5c0",
    "zh:bfd17635f3a9e35362d112483d226d9e4dcd94556af5b06b1127a843adc132f9",
    "zh:d03ed0ece4438b8377ba5442f7f8d09e1aae0b04d47b39ba56e10379ed562d16",
    "zh:d72a78c99806cdf305c39c684882200b1e4cc64a27c7e11f8383545537102977",
    "zh:e7ed250128ce98beea9c9526543dc109f90d085e0f581172f94815496d6bdebd",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version     = "3.66.1"
  constraints = ">= 3.32.0, < 4.0.0"
  hashes = [
    "h1:/VaD9U1DEEW80IOTej91ivCMvNgu455dlAYEPgwnXAY=",
    "zh:36b0f8667d36154fa44337ee153fa425fe4b9f3e1790b81e155100b72499f115",
    "zh:4bd5e445ea4da0245dbdf596cbd8e3da5dd824985cf5de4d5d5f48c323e86a88",
    "zh:4de6b35d9c4e48209eaeaf0e32b94007800097350694050831fab15bf92943a4",
    "zh:7448b97f3de39717a92ac81a6e35026106d6fb952a734b59d3800fd481398aed",
    "zh:75f9eb23773b188325863752e93c753e83f7f5b6f7efd3ef54a0eac6340e1de1",
    "zh:a21e84166be0ff9e85eff4a4e718b11d6defec3bece74df4d9b2df406098fb42",
    "zh:ba89d13119841da3ecc14519a22eef9fbb3b60f8fdf7137e8557fc11969c0ab3",
    "zh:cbd4c39392cbce96f607b21419e8e434bc13e81d90a1b2b7ce10f7d8fd681b92",
    "zh:d38777b256161509c8e24b002a4d731c5027660cc9c4c26aacf22bd186f90ab6",
    "zh:ea7a9a6c2566fbd36b12f48a773a8efbe7d691510e65a5b342cfee78d7f19646",
    "zh:fb3a3186495319709b0794f9d2d7a1946a44d246e9f5c5cdb9893a7e26de30bb",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version = "3.1.0"
  hashes = [
    "h1:XTU9f6sGMZHOT8r/+LWCz2BZOPH127FBTPjMMEAAu1U=",
    "zh:3d46616b41fea215566f4a957b6d3a1aa43f1f75c26776d72a98bdba79439db6",
    "zh:623a203817a6dafa86f1b4141b645159e07ec418c82fe40acd4d2a27543cbaa2",
    "zh:668217e78b210a6572e7b0ecb4134a6781cc4d738f4f5d09eb756085b082592e",
    "zh:95354df03710691773c8f50a32e31fca25f124b7f3d6078265fdf3c4e1384dca",
    "zh:9f97ab190380430d57392303e3f36f4f7835c74ea83276baa98d6b9a997c3698",
    "zh:a16f0bab665f8d933e95ca055b9c8d5707f1a0dd8c8ecca6c13091f40dc1e99d",
    "zh:be274d5008c24dc0d6540c19e22dbb31ee6bfdd0b2cddd4d97f3cd8a8d657841",
    "zh:d5faa9dce0a5fc9d26b2463cea5be35f8586ab75030e7fa4d4920cd73ee26989",
    "zh:e9b672210b7fb410780e7b429975adcc76dd557738ecc7c890ea18942eb321a5",
    "zh:eb1f8368573d2370605d6dbf60f9aaa5b64e55741d96b5fb026dbfe91de67c0d",
    "zh:fc1e12b713837b85daf6c3bb703d7795eaf1c5177aebae1afcf811dd7009f4b0",
  ]
}
