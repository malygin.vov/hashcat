locals {
  k8s-nodes = concat(
    google_compute_instance.kubmst,
    google_compute_instance.kubwrk
  )
  gce_ssh_pub_key_files = [
    "~/.ssh/id_rsa.pub",
    "./pubkey.pub"
  ]
  ssh_pub_keys = join("\n", [
    for filename in local.gce_ssh_pub_key_files: "root:${trimspace(file(filename))}"
  ])
  #gce_ssh_pub_key_file = "./id_rsa.pub"

  rabbitmq_external_admin_port      = 31673
  rabbitmq_external_admin_port_name = "rabbitmq-admin-port"
  rabbitmq_external_amqp_port       = 30673
  rabbitmq_external_amqp_port_name  = "rabbitmq-amqp-port"

  k8s_dashboard_port = 31888
}

output "k8s_nodes" {
  value = {
    for node in local.k8s-nodes :
    node.name => "ansible_host=${node.network_interface[0].access_config[0].nat_ip} ip=${node.network_interface[0].network_ip}"
  }
}

output "rabbitmq_admin_url" {
  value = "http://${module.lb-http-rabbitmq-admin.external_ip}:80"
}

output "k8s_dashboard_url" {
  value = "https://${module.lb-https-k8s-dashboard.external_ip}:443"
}
