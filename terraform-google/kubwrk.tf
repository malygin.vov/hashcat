resource "google_compute_instance_group" "kubwrk" {
  name      = "kubwrk"
  zone      = local.google_zone
  instances = google_compute_instance.kubwrk[*].self_link

  lifecycle {
    create_before_destroy = true
  }
  named_port {
    name = "rabbitmq-admin-port"
    port = local.rabbitmq_external_admin_port
  }
  named_port {
    name = "rabbitmq-amqp-port"
    port = local.rabbitmq_external_amqp_port
  }
}

resource "google_compute_instance" "kubwrk" {
  count        = 5
  name         = "kubwrk${format("%02d", count.index + 1)}"
  machine_type = "e2-highcpu-4"
  zone         = local.google_zone

  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
      size  = 20
    }
  }

  metadata = {
    sshKeys = local.ssh_pub_keys
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

}

# resource "google_compute_instance" "kubwrk-gpu" {
#   count        = 1
#   name         = "kubwrk-gpu-${format("%02d", count.index + 1)}"
#   machine_type = "a2-highgpu-1g"


#   boot_disk {
#     initialize_params {
#       image = "ubuntu-os-cloud/ubuntu-1804-lts"
#       size  = 20
#     }
#   }

#   metadata = {
#     sshKeys = local.ssh_pub_keys
#   }

#   network_interface {
#     network = "default"

#     access_config {
#       // Ephemeral IP
#     }
#   }

# }

module "lb-http-rabbitmq-admin" {
  source         = "GoogleCloudPlatform/lb-http/google"
  version        = "~> 4.4"
  name           = "rabbitmq-admin-http"
  create_address = true
  project        = local.google_project
  backends = {
    default = {
      protocol               = "HTTP"
      description            = "RabbitMQ Admin Panel"
      port                   = local.rabbitmq_external_admin_port
      port_name              = local.rabbitmq_external_admin_port_name
      timeout_sec            = 5
      enable_cdn             = false
      custom_request_headers = null
      security_policy        = null

      connection_draining_timeout_sec = null
      session_affinity                = null
      affinity_cookie_ttl_sec         = null
      health_check = {
        request_path        = "/"
        port                = local.rabbitmq_external_admin_port
        check_interval_sec  = null
        timeout_sec         = null
        healthy_threshold   = null
        unhealthy_threshold = null
        host                = null
        logging             = null
      }
      log_config = {
        enable      = true
        sample_rate = 1.0
      }
      groups = [
        {
          group                        = google_compute_instance_group.kubwrk.id
          balancing_mode               = null
          capacity_scaler              = null
          description                  = null
          max_connections              = null
          max_connections_per_instance = null
          max_connections_per_endpoint = null
          max_rate                     = null
          max_rate_per_instance        = null
          max_rate_per_endpoint        = null
          max_utilization              = null
        }
      ]
      iap_config = {
        enable               = false
        oauth2_client_id     = null
        oauth2_client_secret = null
      }
    }
  }
}
